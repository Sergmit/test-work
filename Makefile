up: docker-up
down: docker-down


docker-up:
	docker-compose up -d

docker-down:
	docker-compose down --remove-orphans

docker-down-clear:
	docker-compose down -v --remove-orphans

docker-pull:
	docker-compose pull

docker-build:
	docker-compose build

docker-logs:
	docker-compose logs

docker-logs-follow:
	docker-compose logs -f

api-composer-install:
	docker-compose run --rm php composer install

api-composer-update:
	docker-compose run --rm php composer update

api-migrate-db:
	docker-compose exec php php artisan migrate

api-migrate-fresh-db:
	docker-compose exec php php artisan migrate:fresh

api-db-seed:
	docker-compose exec php php artisan db:seed

api-cache-clear:
	docker-compose exec php php artisan cache:clear


