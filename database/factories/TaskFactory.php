<?php

namespace Database\Factories;

use App\Models\Status;
use App\Models\Task;
use Illuminate\Database\Eloquent\Factories\Factory;

class TaskFactory extends Factory
{
    protected $model = Task::class;

    public function definition(): array
    {
        return [
            'name' => $this->faker->text(50),
            'description' => $this->faker->text(),
            'author' => $this->faker->name(),
            'executor' => $this->faker->name(),
            'status_id' => Status::all()->random(),
            'priority' => rand(0, 20),
            'deadline' => $this->faker->dateTimeBetween('+1 days', '+30days')
        ];
    }
}
