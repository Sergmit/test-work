<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tags', function (Blueprint $table) {
            $table->id();
            $table->string('name');
        });
        DB::table('statuses')->insert([
            ['name' => 'game'],
            ['name' => 'politics'],
            ['name' => 'health']
        ]);
        Schema::create('tag_task', function (Blueprint $table) {
            $table->foreignIdFor(\App\Models\Tag::class);
            $table->foreignIdFor(\App\Models\Task::class);
            $table->foreign('tag_id')->references('id')->on('tags');
            $table->foreign('task_id')->references('id')->on('tasks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task_tag');
        Schema::dropIfExists('tags');
    }
}
