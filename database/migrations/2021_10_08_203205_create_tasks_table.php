<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statuses', function (Blueprint $table) {
            $table->id();
            $table->string('name');
        });
        DB::table('statuses')->insert([
            ['name' => 'open'],
            ['name' => 'in_progress'],
            ['name' => 'closed']
        ]);
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->string('name')->index();
            $table->string('description');
            $table->string('author')->index();
            $table->string('executor')->index();
            $table->foreignIdFor(\App\Models\Status::class);
            $table->mediumInteger('priority')->index();
            $table->mediumInteger('level')->default(1)->index();
            $table->timestamp('deadline')->index();
            $table->unsignedBigInteger('parent_id')->nullable()->index();
            $table->timestamps();
            $table->index('created_at');

            $table->foreign('status_id')->references('id')->on('statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statuses');
        Schema::dropIfExists('tasks');
    }
}
