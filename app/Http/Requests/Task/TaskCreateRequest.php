<?php

namespace App\Http\Requests\Task;

use Anik\Form\FormRequest;
use App\UseCases\Task\Dto\TaskCreateDto;

class TaskCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function rules(): array
    {
        return [
            'tasks' => "required|array",
            'tasks.*.name' => "required|string",
            'tasks.*.description' => "required|string",
            'tasks.*.author' => "required|string",
            'tasks.*.parent_id' => "numeric|nullable",
            'tasks.*.executor' => "required|string",
            'tasks.*.deadline' => "required|date_format:Y-m-d H:i:s",
            'tasks.*.status_id' => "required|exists:statuses,id"
        ];
    }

    /**
     * @return TaskCreateDto
     */
    public function getDto(): TaskCreateDto
    {
        return new TaskCreateDto(
            $this->get('tasks')
        );
    }
}
