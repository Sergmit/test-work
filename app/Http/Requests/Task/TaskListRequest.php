<?php

namespace App\Http\Requests\Task;

use Anik\Form\FormRequest;
use App\UseCases\Task\Dto\TaskListDto;
use Illuminate\Validation\Rule;

class TaskListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function rules(): array
    {
        return [
            'order' => "nullable|array",
            'order.*.name' => "required|string",
            'order.*.dir' => ["required", Rule::in(['asc', 'desc'])],
            'filter' => "nullable|array",
            'filter.author' => "string",
            'filter.executor' => "string",
            'filter.name' => "string",
            'filter.priority' => "string",
            'filter.deadline' => "string",
            'filter.tag' => "string",
            'author' => "nullable|string",
            'executor' => "nullable|string",
            'name' => "nullable|string",
            'priority' => "nullable|string",
            'deadline' => "nullable|string",
            'tag' => "nullable|exists:tags,id"
        ];
    }

    public function getDto(): TaskListDto
    {
        return new TaskListDto(
            $this->get('order', []),
            $this->get('filter', []),
            $this->get('name'),
            $this->get('author'),
            $this->get('executor'),
            $this->get('priority'),
            $this->get('deadline'),
            $this->get('tag')
        );
    }
}
