<?php

namespace App\Http\Requests\Status;

use Anik\Form\FormRequest;
use App\UseCases\Status\Dto\CreateDto;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function rules(): array
    {
        return [
            'name' => 'required|string:100'
        ];
    }

    /**
     * @return CreateDto
     */
    public function getDto(): CreateDto
    {
        return new CreateDto($this->get('name'));
    }
}
