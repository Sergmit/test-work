<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * @param array $data
     * @param string $msg
     * @param array $errors
     * @return array[]
     */
    public function returnData(array $data = [], string $msg = 'ok', array $errors = []): array
    {
        return ['data' => $data, 'errors' => $errors];
    }
}
