<?php

namespace App\Http\Controllers;

use App\Http\Requests\Task\TaskCreateRequest;
use App\Http\Requests\Task\TaskListRequest;
use App\Repositories\TaskRepository;
use App\UseCases\Task\GetListUseCase;
use App\UseCases\Task\TaskCreateUseCase;

class TaskController extends Controller
{
    /**
     * @param $id
     * @param TaskRepository $taskRepository
     * @return array
     */
    public function getTaskById($id, TaskRepository $taskRepository): array
    {
        $task = $taskRepository->getById($id);

        return $this->returnData(['task' => $task]);
    }

    /**
     * @param TaskListRequest $request
     * @param GetListUseCase $useCase
     * @return array
     * @throws \Exception
     */
    public function getList(TaskListRequest $request, GetListUseCase $useCase): array
    {

        $tasks = $useCase->handle($request->getDto());

        return $this->returnData(['tasks' => $tasks]);
    }

    /**
     * @param TaskCreateRequest $request
     * @param TaskCreateUseCase $useCase
     * @return array[]
     * @throws \Exception
     */
    public function create(TaskCreateRequest $request, TaskCreateUseCase $useCase)
    {
        $tasks = $useCase->handle($request->getDto());

        return $this->returnData(['tasks' => $tasks]);
    }
}
