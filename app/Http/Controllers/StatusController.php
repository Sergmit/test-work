<?php

namespace App\Http\Controllers;

use App\Http\Requests\Status\CreateRequest;
use App\UseCases\Status\CreateUseCase;

class StatusController extends Controller
{
    /**
     * Create status
     * @param CreateRequest $request
     * @param CreateUseCase $useCase
     * @return array
     */
    public function create(CreateRequest $request, CreateUseCase $useCase): array
    {
        $status = $useCase->handle($request->getDto());

        return $this->returnData(['status_id' => $status->id]);
    }
}
