<?php

namespace App\Console\Commands;

use App\Services\TasksExportCsvService;
use App\Services\TasksExportPdfService;
use Illuminate\Console\Command;

class ExportTasks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:tasks {type=csv}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export tasks';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param TasksExportCsvService $tasksExportCsvService
     * @param TasksExportPdfService $tasksExportPdfService
     * @return bool
     * @throws \Exception
     */
    public function handle(TasksExportCsvService $tasksExportCsvService, TasksExportPdfService $tasksExportPdfService): bool
    {
        $type = $this->argument('type');

        switch ($type) {
            case 'csv':
                $tasksExportCsvService->export();
                break;
            case 'pdf':
                $tasksExportPdfService->export();
                break;
            default:
                throw new \Exception("Service for $type not found");
        }
        return true;
    }
}
