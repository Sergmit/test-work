<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $name
 * @property int $id
 */
class Status extends Model
{
    public $timestamps = false;
}
