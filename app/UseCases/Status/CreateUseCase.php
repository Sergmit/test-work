<?php
declare(strict_types=1);

namespace App\UseCases\Status;

use App\Models\Status;
use App\UseCases\Status\Dto\CreateDto;

class CreateUseCase
{

    public function handle(CreateDto $dto): Status
    {
        $status = new Status();
        $status->name = $dto->getName();
        $status->save();

        return $status;
    }
}
