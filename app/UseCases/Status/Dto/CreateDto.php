<?php
declare(strict_types=1);

namespace App\UseCases\Status\Dto;

class CreateDto
{
    private string $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
