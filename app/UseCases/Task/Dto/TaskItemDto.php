<?php
declare(strict_types=1);

namespace App\UseCases\Task\Dto;

class TaskItemDto
{
    private string $name;
    private string $author;
    private string $executor;
    private int $priority;
    private string $deadline;
    private string $description;
    private ?int $parentId;
    private int $statusId;

    public function __construct(string $name, string $description, string $author, string $executor, int $priority, string $deadline, int $statusId, ?int $parentId = null)
    {

        $this->name = $name;
        $this->author = $author;
        $this->executor = $executor;
        $this->priority = $priority;
        $this->deadline = $deadline;
        $this->description = $description;
        $this->parentId = $parentId;
        $this->statusId = $statusId;
    }


    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getAuthor(): string
    {
        return $this->author;
    }

    /**
     * @return string
     */
    public function getExecutor(): string
    {
        return $this->executor;
    }

    /**
     * @return int
     */
    public function getPriority(): int
    {
        return $this->priority;
    }

    /**
     * @return string
     */
    public function getDeadline(): string
    {
        return $this->deadline;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return int|null
     */
    public function getParentId(): ?int
    {
        return $this->parentId;
    }

    /**
     * @return int
     */
    public function getStatusId(): int
    {
        return $this->statusId;
    }


}
