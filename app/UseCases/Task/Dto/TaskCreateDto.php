<?php
declare(strict_types=1);

namespace App\UseCases\Task\Dto;

class TaskCreateDto
{
    private array $tasks;

    public function __construct(array $tasks)
    {

        $this->tasks = array_map(function ($item) {
            return new TaskItemDto($item['name'], $item['description'], $item['author'], $item['executor'], $item['priority'], $item['deadline'], $item['status_id'], $item['parent_id']);
        }, $tasks);
    }

    /**
     * @return TaskItemDto[]|array
     */
    public function getTasks(): array
    {
        return $this->tasks;
    }
}
