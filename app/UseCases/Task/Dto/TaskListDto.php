<?php
declare(strict_types=1);

namespace App\UseCases\Task\Dto;

class TaskListDto
{
    private ?array $order;
    private ?string $name;
    private ?string $author;
    private ?string $executor;
    private ?string $priority;
    private ?string $deadline;
    private ?array $filter;
    private ?string $tag;

    public function __construct(?array $order, ?array $filter, ?string $name, ?string $author, ?string $executor, ?string $priority, ?string $deadline, ?string $tag)
    {

        $this->order = $order;
        $this->name = $name;
        $this->author = $author;
        $this->executor = $executor;
        $this->priority = $priority;
        $this->deadline = $deadline;
        $this->filter = $filter;
        $this->tag = $tag;
    }

    /**
     * @return array
     */
    public function getOrder(): ?array
    {
        return $this->order;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @return mixed
     */
    public function getExecutor()
    {
        return $this->executor;
    }

    /**
     * @return mixed
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @return mixed
     */
    public function getDeadline()
    {
        return $this->deadline;
    }

    /**
     * @return array|null
     */
    public function getFilter(): ?array
    {
        return $this->filter;
    }

    /**
     * @return string|null
     */
    public function getTag(): ?string
    {
        return $this->tag;
    }
}
