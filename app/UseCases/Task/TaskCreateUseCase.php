<?php
declare(strict_types=1);

namespace App\UseCases\Task;

use App\Models\Task;
use App\Repositories\TaskRepository;
use App\UseCases\Task\Dto\TaskCreateDto;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class TaskCreateUseCase
{
    private TaskRepository $repository;

    public function __construct(TaskRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param TaskCreateDto $dto
     * @return Collection
     * @throws \Exception
     */
    public function handle(TaskCreateDto $dto): Collection
    {
        $tasks = $dto->getTasks();
        $outcome = new Collection();
        DB::beginTransaction();
        try {
            foreach ($tasks as $item) {
                $parent_id = $item->getParentId();
                $task = new Task();
                if ($parent_id) {
                    $parentTask = $this->repository->getById($parent_id);
                    if (empty($parentTask)) {
                        throw new \DomainException("Parent task not found");
                    }
                    $task->parent_id = $parentTask->id;
                    $task->level = $parentTask->level + 1;
                }
                $task->name = $item->getName();
                $task->description = $item->getDescription();
                $task->author = $item->getAuthor();
                $task->executor = $item->getExecutor();
                $task->deadline = $item->getDeadline();
                $task->priority = $item->getPriority();
                $task->status_id = $item->getStatusId();
                $task->save();
                $task->load('status');
                $outcome->add($task);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            throw new \Exception($e->getMessage());
        }
        DB::commit();
        return $outcome;
    }
}
