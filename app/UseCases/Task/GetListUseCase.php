<?php
declare(strict_types=1);

namespace App\UseCases\Task;

use App\Repositories\TaskRepository;
use App\UseCases\Task\Dto\TaskListDto;
use Illuminate\Database\Eloquent\Collection;

class GetListUseCase
{
    /**
     * @var TaskRepository
     */
    private TaskRepository $repository;

    public function __construct(TaskRepository $repository)
    {
        $this->repository = $repository;
    }

    /**'
     * @param TaskListDto $dto
     * @return Collection
     * @throws \Exception
     */
    public function handle(TaskListDto $dto): Collection
    {
        return $this->repository->getList($dto);
    }
}
