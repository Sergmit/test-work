<?php
declare(strict_types=1);

namespace App\Repositories;

use App\Models\Task;
use App\UseCases\Task\Dto\TaskListDto;
use Illuminate\Database\Eloquent\Collection;

class TaskRepository
{

    /**
     * @param TaskListDto $dto
     * @return Collection
     * @throws \Exception
     */
    public function getList(TaskListDto $dto): Collection
    {
        $builder = Task::with(['status', 'tags']);
        if ($dto->getFilter()) {
            $filter = $dto->getFilter();
            $builder->where(function ($query) use ($filter) {
                foreach ($filter as $field => $value) {
                    $query->orWhere($field, $value);
                }
            });
        }

        $builder->when($dto->getName(), function ($query, $name) {
            $query->where('name', $name);
        });
        $builder->when($dto->getAuthor(), function ($query, $author) {
            $query->where('author', $author);
        });
        $builder->when($dto->getExecutor(), function ($query, $executor) {
            $query->where('executor', $executor);
        });
        $builder->when($dto->getDeadline(), function ($query, $deadline) {
            $dateStart = (new \DateTime($deadline))->format('Y-m-d 00:00:00');
            $dateEnd = (new \DateTime($deadline))->format('Y-m-d 23:59:59');
            $query->whereBetween('deadline', [$dateStart, $dateEnd]);
        });
        $builder->when($dto->getPriority(), function ($query, $priority) {
            $query->where('priority', $priority);
        });

        $builder->when($dto->getTag(), function ($query, $tag) {
            $query->whereHas('tags', function ($query) use ($tag) {
                $query->where('tag_id', $tag);
            });
        });

        foreach ($dto->getOrder() as $order) {
            $builder->orderBy($order['name'], $order['dir']);
        }
        return $builder->get();
    }

    /**
     * Get task by id
     * @param int $id
     * @return Task
     */
    public function getById(int $id): Task
    {
        return Task::findOrFail($id);
    }

    public function getTasksForExport()
    {
        return Task::with(['status' => function ($query) {
            $query->where(['name' => 'closed']);
        }])
            ->where('level', '>', 3)->get();
    }

}
