<?php
declare(strict_types=1);

namespace App\Services;

use App\Repositories\TaskRepository;

abstract class TasksExportAbstract
{
    protected TaskRepository $repository;
    protected $tasks = null;

    public function __construct(TaskRepository $repository)
    {
        $this->tasks = $repository->getTasksForExport();
    }

    public abstract function export();

}
