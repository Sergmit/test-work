<?php
declare(strict_types=1);

namespace App\Services;

class TasksExportCsvService extends TasksExportAbstract
{
    public function export(): bool
    {
        $datetime = (new \DateTime())->format('Y-m-d H:i:s');
        $filename = "tasks_$datetime.csv";
        $handle = fopen(storage_path('app/tasks') . $filename, 'w');
        fputcsv($handle, ['name', 'description', 'author', 'executor', 'deadline', 'priority']);

        foreach ($this->tasks as $task) {

            fputcsv($handle, [$task->name, $task->description, $task->author, $task->executor, $task->deadline, $task->priority]);
        }
        fclose($handle);
        return true;
    }
}
